<?php

namespace Spip\Tools\Archives\Command;

use DateTime;
use Spip\Tools\Archives\ArchiveException;


class ArchiveCheckout extends ArchiveBase {

    protected static $defaultName = 'archive:checkout';
    protected static $defaultDescription = 'Génère une archive d’un SPIP (avec Checkout)';

    protected $componentDir = 'checkout/';

    protected function download(string $version, bool $noDownload = false, bool $interactive = true) {
        $source = $this->getSourceDir($version);
        $this->io->section("Checkout $version in $source");
        if ($noDownload) {
            if (is_file($source . '.git/config')) {
                $this->io->text("! <comment>Checkout ignoré.</comment>");
                return;
            }
        }

        $this->createDirectory(dirname($source));
        $cmd = "checkout spip -b'$version' '" . rtrim($source, '/') . "'";
        passthru($cmd);
    }

    protected function build(string $version, bool $interactive = true) {
        $destination = $this->getBuildDir($version);
        $source = $this->getSourceDir($version);

        $this->io->section("Build $version in $destination");

        $this->createDirectory($destination);
        $this->createDirectory($source);

        $params = $interactive ? '' : ' --no-interaction';
        $composer_file = $source . 'composer.json';
        if (!file_exists($composer_file)) {
            $template = $this->getPathTemplateComposer();
            $this->copyTemplate($template, $composer_file, $version, $interactive);
    
            $cmd = "cd $source && composer $params spip-archive";    
        } else {
            $cmd = "cd $source && composer $params archive --format=zip --dir=" . realpath($destination) . " --file=" . $this->getZipName($version);
        }

        passthru($cmd);

        $this->io->writeln("");
        $this->timestampArchive($version);
    }

    protected function timestampArchive(string $version) {
        $archiveFile = $this->getBuildFile($version);
        $this->io->section("Change archive date");

        $sourceDir = $this->getSourceDir($version);
        $timestamp = $this->getTimestampLastCommitInSpipProject($sourceDir);
        $date = (new DateTime())->setTimestamp($timestamp)->format("Y-m-d H:i:s");
        $this->io->text("Last commit date : <info>" . $date . "</info>");
        if (file_exists($archiveFile)) {
            touch($archiveFile, $timestamp);
            clearstatcache(true, $archiveFile);
            if ($timestamp == filemtime($archiveFile)) {
                $this->io->text("Date set to : <info>" . $date . "</info>");
            } else {
                $this->io->warning("Date has not been set to : $date" );
            }
        } else {
            $this->io->warning("No archive found. Can't change archive date.");
        }
    }

    protected function getTimestampLastCommitInSpipProject(string $sourceDir) : int {
        $timestamp = $this->getTimestampLastCommit($sourceDir);
        foreach ($this->getPluginsPath($sourceDir) as $pluginDir) {
            $t = $this->getTimestampLastCommit($pluginDir);
            if ($t > $timestamp) {
                $timestamp = $t;
            }
        }
        return $timestamp;
    }

    protected function getPluginsPath(string $rootDir) : array {
        $list = [];
        $directories = new \FilesystemIterator($rootDir . '/plugins-dist', \FilesystemIterator::SKIP_DOTS);
        foreach ($directories as $directory) {
            if ($directory->isDir()) {
                $list[] = $directory->getPathname();
            }
        }
        $list[] = $rootDir . '/squelettes-dist';
        sort($list);
        return $list;
    }

    protected function getTimestampLastCommit(string $directory) : int {
        return (int) trim(shell_exec("cd $directory && git log --pretty=tformat:'%ct' -1"));
    }
}
