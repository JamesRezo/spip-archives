<?php

namespace Spip\Tools\Archives;

use Symfony\Component\Console\Application as ConsoleApplication;
use Symfony\Component\Console\Input\InputInterface;


class Application extends ConsoleApplication {

	const NAME = "Spip Archives";
	const VERSION = "1.0.3";

	/**
	 * Application constructor.
	 *
	 * Permet d’appliquer une commande à un ensemble de sites
	 * d’une mutualisation.
	 *
	 * @param array $options
	 */
	public function __construct(array $options = []) {
		parent::__construct(self::NAME, self::VERSION);

		$this->add(new Command\ArchiveCheckout());
		$this->add(new Command\ArchiveComposer());
	}
}