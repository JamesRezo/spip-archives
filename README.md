# Générer une archive pour SPIP

## Options

Deux choix testés :

- checkout : crée le SPIP avec [checkout](https://git.spip.net/spip-contrib-outils/checkout)
- composer : crée le SPIP avec Composer ; en utilisant un [repository très spécifique](https://gitlab.com/magraine/spip-releases)

## Commandes

- `composer spip-archives-checkout` est un alias de `php bin/spip-archives archive:checkout`
- `composer spip-archives-composer` est un alias de `php bin/spip-archives archive:composer`

Pour passer des paramètres contenant `--` il faut forcément transmettre un `--` vide avant.
Exemple `composer spip-archives-checkout -- --no-download`

Les commandes sont identiques pour `spip-archives-checkout` ou `spip-archives-composer`.

### checkout

```sh
# Génère build/checkout/SPIP-master.zip
composer spip-archives-checkout
composer spip-archives-checkout master

# Génère build/checkout/SPIP-branche-3.2.zip
composer spip-archives-checkout 3.2

# Génère build/checkout/SPIP-v3.2.7.zip
composer spip-archives-checkout 3.2.7

# Ignorer le téléchargement ou mise à jour si la source existe déjà
composer spip-archives-checkout -- --no-download

# Ignorer le mode interactif des appels à composer
composer --no-interaction spip-archives-checkout 3.2.8 -- --no-interaction
```

### composer

```sh
# Génère build/composer/SPIP-master.zip
composer spip-archives-composer
composer spip-archives-composer master

# Génère build/composer/SPIP-branche-3.2.zip
composer spip-archives-composer 3.2

# Génère build/composer/SPIP-v3.2.7.zip
composer spip-archives-composer 3.2.7

# Ignorer le téléchargement ou mise à jour si la source existe déjà
composer spip-archives-composer -- --no-download

# Ignorer le mode interactif des appels à composer
composer --no-interaction spip-archives-composer 3.2.8 -- --no-interaction
```
